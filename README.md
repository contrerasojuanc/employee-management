NS - Test - PHP/MVC - JuanContreras
========================

Test Employee App allows all CRUD operations over employees

Deploy
------------
To test this app without doing any installation, please go to this link: 
```bash
https://nameless-crag-51200.herokuapp.com/es/
```

Installation
------------

First, execute next command:

```bash
$ composer install
```

Second, set local mysql database  connection string (e.g. mysql://root:123456@127.0.0.1:3306/employeedb) on ../app/config/parameters.yml file

Third, execute next commands:
```bash
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:create
$ php bin/console doctrine:fixtures:load
```

Usage
-----

There is no need to configure a virtual host in your web server to access the application.
Just use the built-in web server:

```bash
$ php bin/console server:run
```

This command will start a web server for the Symfony application. Now you can
access the application in your browser at <http://localhost:8000>. You can
stop the built-in web server by pressing `Ctrl + C` while you're in the
terminal.

Now, access to the app using user:admin and password:admin