<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Employee;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Defines the sample to load in the database before running the unit
 * and functional tests. Execute this command to load the data.
 *
 *   $ php bin/console doctrine:fixtures:load
 *
 * See http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 */
class EmployeeFixtures extends AbstractFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        
        for ($i=0; $i < 30; $i++) {
            $employee = new Employee();
            $employee->setId($faker->numerify('V########'));
            $employee->setFullName($faker->name);
            $employee->setPhone($faker->numerify('###########'));
            $employee->setAddress($faker->address);
            $employee->setEmail($faker->email);
            $employee->setContractDate($faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = date_default_timezone_get()));
            $employee->setBirthDate($faker->dateTimeBetween($startDate = '-40 years', $endDate = 'now', $timezone = date_default_timezone_get()));
            $employee->setIsFreelancer($faker->boolean);
            $employee->setHourRate($faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100));

            $manager->persist($employee);
        }
        $manager->flush();
    }
}
