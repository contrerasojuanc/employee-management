<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Employee
 *
 * @ORM\Table(name="employee")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * Pagination limit
     *
     */
    const NUM_ITEMS = 5;
    
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=15)
     * @ORM\Id
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[Vv]\d+$/i",
     *     match=true,
     *     message="Your id must have the next format: V1234567"
     * )
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=500, nullable=true)
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=true)
     * @Assert\Regex(
     *     pattern="/\d{11}$/",
     *     match=true,
     *     message="Your phone must have 11 digits"
     *  )
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contract_date", type="datetime", nullable=false)
     */
    private $contractDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     */
    private $birthDate = 'CURRENT_TIMESTAMP';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_freelancer", type="boolean", nullable=true)
     */
    private $isFreelancer = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="hour_rate", type="decimal", nullable=true)
     */
    private $hourRate = '0';
    

    public function __construct() {
        $this->contractDate = new \DateTime();
        $this->birthDate = new \DateTime();
        $this->isFreelancer = false;
    }
    
    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Employee
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Employee
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Employee
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Employee
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set contractDate
     *
     * @param \DateTime $contractDate
     *
     * @return Employee
     */
    public function setContractDate($contractDate)
    {
        $this->contractDate = $contractDate;

        return $this;
    }

    /**
     * Get contractDate
     *
     * @return \DateTime
     */
    public function getContractDate()
    {
        return $this->contractDate;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Employee
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set isFreelancer
     *
     * @param boolean $isFreelancer
     *
     * @return Employee
     */
    public function setIsFreelancer($isFreelancer)
    {
        $this->isFreelancer = $isFreelancer;

        return $this;
    }

    /**
     * Get isFreelancer
     *
     * @return boolean
     */
    public function getIsFreelancer()
    {
        return $this->isFreelancer;
    }

    /**
     * Set hourRate
     *
     * @param integer $hourRate
     *
     * @return Employee
     */
    public function setHourRate($hourRate)
    {
        $this->hourRate = $hourRate;

        return $this;
    }

    /**
     * Get hourRate
     *
     * @return integer
     */
    public function getHourRate()
    {
        return $this->hourRate;
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Employee
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
