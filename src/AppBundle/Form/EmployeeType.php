<?php

namespace AppBundle\Form;

use AppBundle\Entity\Employee;
use AppBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Defines the form used to create and manipulate employees.
 *
 */
class EmployeeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // For the full reference of options defined by each form field type
        // see http://symfony.com/doc/current/reference/forms/types.html

        // By default, form fields include the 'required' attribute, which enables
        // the client-side form validation. This means that you can't test the
        // server-side validation errors from the browser. To temporarily disable
        // this validation, set the 'required' attribute to 'false':
        // $builder->add('title', null, ['required' => false, ...]);

        $builder
            ->add('id', null, [
                'attr' => ['autofocus' => true],
                'label' => 'label.id',
            ])
            ->add('fullName', null, [
                'label' => 'label.employee_fullname',
            ])
            ->add('email', null, [
                'label' => 'label.email',
                'required' => false,
            ])
            ->add('address', TextareaType::class, [
                'label' => 'label.address',
            ])
            ->add('phone', null, [
                'label' => 'label.phone',
                'required' => false,
            ])
            ->add('birthDate', DateTimePickerType::class, [
                'label' => 'label.birth_date',
                'required' => false,
            ])
            ->add('contractDate', DateTimePickerType::class, [
                'label' => 'label.contract_date',
            ])
            ->add('isFreelancer', ChoiceType::class, [
                'label' => 'label.is_freelancer',
                'choices'  => array(
                    'label.is_frelancer_yes' => 1,
                    'label.is_frelancer_no' => 0,
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
            ])
            ->add('hourRate', MoneyType::class, [
                'label' => 'label.hour_rate',
                'currency' => 'USD',
                'required' => false,
            ])
        ;
        
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
