<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Employee;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for employee information.
 *
 */
class EmployeeRepository extends EntityRepository
{
    /**
     * @return Query
     */
    public function queryOrdered()
    {
        return $this->getEntityManager()
            ->createQuery('
                SELECT e
                FROM AppBundle:Employee e
                ORDER BY e.contractDate DESC
            ')
        ;
    }
    
    /**
     * @return Query
     */
    public function queryOrderedAndOrdered(Request $request)
    {
        $searchText = $request->get('search_text', null);
        $searchFrom = $request->get('search_from', null);
        $searchTo = $request->get('search_to', null);
        
        $query = $this->createQueryBuilder("e");
        
        if ($searchText) {
            $query = $query->andWhere("e.id like '%$searchText%'")
                ->orWhere("e.fullName like '%$searchText%'")
                ->orWhere("e.email like '%$searchText%'")
                ->orWhere("e.address like '%$searchText%'");
        }
        if ($searchFrom) {
            $today_startdatetime = new \DateTime($searchFrom);
            $query = $query->andWhere("e.contractDate >= :searchFrom");
            $query = $query->setParameter('searchFrom', $today_startdatetime);
        }
        if ($searchTo) {
            $today_enddatetime = new \DateTime($searchTo);
            $query = $query->andWhere("e.contractDate <= :searchTo");
            $query = $query->setParameter('searchTo', $today_enddatetime);
        }
        
        $query = $query->orderBy('e.contractDate', 'desc');

        $result = $query->getQuery();
        
        return $result;
    }

    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findLatest($page = 1)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($this->queryOrdered(), false));
        $paginator->setMaxPerPage(Employee::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function searchLatest($page = 1,Request $request)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($this->queryOrderedAndOrdered($request), false));
        $paginator->setMaxPerPage(Employee::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
