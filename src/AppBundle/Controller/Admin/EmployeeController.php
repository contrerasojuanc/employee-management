<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Employee;
use AppBundle\Form\EmployeeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Controller used to manage employees.
 *
 * @Route("/")
 * @Security("has_role('ROLE_ADMIN')")
 *
 */
class EmployeeController extends Controller
{
    /**
     * Lists all Employee entities.
     *
     * @Route("/", defaults={"page": "1"}, name="index")
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="index_paginated")
     * @Method("GET")
     * @Cache(smaxage="5")
     */
    public function indexAction($page)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $employees = $entityManager->getRepository(Employee::class)->findLatest($page);

        return $this->render('admin/employee/index.html.twig', ['employees' => $employees]);
    }

    /**
     * Creates a new Employee entity.
     *
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     *
     */
    public function newAction(Request $request)
    {
        $employee = new Employee();

        // See http://symfony.com/doc/current/book/forms.html#submitting-forms-with-multiple-buttons
        $form = $this->createForm(EmployeeType::class, $employee)
            ->add('saveAndCreateNew', SubmitType::class);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See http://symfony.com/doc/current/best_practices/forms.html#handling-form-submits
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($employee);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See http://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'employee.created_successfully');

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('new');
            }

            return $this->redirectToRoute('index');
        }

        return $this->render('admin/employee/new.html.twig', [
            'employee' => $employee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Employee entity.
     *
     * @Route("/{id}/edit", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Employee $employee, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', $employee, 'Employees can only be edited by administrators.');

        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(EmployeeType::class, $employee);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $this->addFlash('success', 'employee.updated_successfully');

            return $this->redirectToRoute('index');
        }

        return $this->render('admin/employee/edit.html.twig', [
            'employee' => $employee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Employee entity.
     *
     * @Route("/{id}/delete", name="delete")
     * @Method("POST")
     *
     */
    public function deleteAction(Request $request, Employee $employee)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('index');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($employee);
        $entityManager->flush();

        $this->addFlash('success', 'employee.deleted_successfully');

        return $this->redirectToRoute('index');
    }

    /**
     * Employee search response to a ajax request.
     *
     * @Route("/search", name="search")
     * @Route("/search/page/{page}", requirements={"page": "[1-9]\d*"}, name="search_paginated")
     * @Method("POST")
     * @Cache(smaxage="5")
     *
     */
    public function searchAction(Request $request, $page = 1)
    {
        $response = new JsonResponse();
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $entityManager = $this->getDoctrine()->getManager();
        $employees =  $entityManager->getRepository(Employee::class)->searchLatest($page, $request);
        
        // Generates the table actions buttons
        $templates = [];
        foreach($employees as $employee) {
            $templates[] = [ 'id' => $employee->getId(), 'html' => $this->render('admin/employee/_table_actions.html.twig', ['employee' => $employee->getId()]) ];
        }
        
        $response = new JsonResponse();
        $response->setStatusCode(200);
        $response->setData(array(
            'response' => 'success',
            'employees' => $serializer->serialize($employees, 'json'),
            'templates' => $serializer->serialize($templates, 'json'),
        ));
        return $response;
    }
}
